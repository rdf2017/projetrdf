package projetRDFmain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FeaturesController {
	final private List<String> m_featuresEnabled;
	final private String m_folderPath;
	private List<List<Double>> compiledData;
	
	public FeaturesController(List<String> featuresEnabled, String folderPath) {
		m_featuresEnabled=featuresEnabled;
		m_folderPath = folderPath;
	}
	
	public List<List<Double>> obtainAndCompileData() {
		final CSVReader reader = new CSVReader();
		compiledData = null;
		// create as many records as there are in feature files
		boolean firstTime = true;
		for(String f : m_featuresEnabled) {
			final String filePath = m_folderPath + "/" + f;
			try {
				final List<List<Double>> filesOfFeature = reader.readAll(filePath);
				// setup de la liste de retour si premi�re feature
				if (firstTime) {
					compiledData = new ArrayList<List<Double>>(filesOfFeature.size());
					for(@SuppressWarnings("unused") List<Double> file : filesOfFeature) {
						compiledData.add(new ArrayList<Double>(50));
					}
					firstTime = false;
				}
				
				// normalisation des records de la feature : trouver min, max, et ajuster entre 0 et 100
				final double[] min = new double[filesOfFeature.get(0).size()];
				final double[] max = new double[min.length];
				Arrays.fill(min, Double.MAX_VALUE);
				Arrays.fill(max, Double.MIN_VALUE);
				for (List<Double> fileOfFeature : filesOfFeature) {	// pour chaque fichier
					for (int dimIdx = 0; dimIdx < fileOfFeature.size(); dimIdx++) {	// pour chaque dimension
						final double dimValue = fileOfFeature.get(dimIdx);	// on cherche min et max
						if (min[dimIdx] > dimValue) {
							min[dimIdx] = dimValue;
						}
						if (max[dimIdx] < dimValue) {
							max[dimIdx] = dimValue;
						}
					}
				}
				for (List<Double> fileOfFeature : filesOfFeature) {	// pour chaque fichier
					for (int dimIdx = 0; dimIdx < fileOfFeature.size(); dimIdx++) {	// pour chaque dimension
						final double newValue = (((fileOfFeature.get(dimIdx) - min[dimIdx]) * (100d - 0d)) / (max[dimIdx] - min[dimIdx])) + 0d;
						fileOfFeature.set(dimIdx, newValue);
					}
				}
				
				// add feature data to the records
				for(int fileIdx = 0; fileIdx < filesOfFeature.size(); fileIdx++) {
					compiledData.get(fileIdx).addAll(filesOfFeature.get(fileIdx));
				}
			} catch(IOException e) {
				System.err.println("Can't load feature \"" + f + "\"");				
			}
			
		}
		return compiledData;
	}

	/**
	 * @return the compiledData
	 */
	protected final List<List<Double>> getCompiledData() {
		return compiledData;
	}
	
}
