package projetRDFmain;

public enum Category {
	JUNGLE(0,99),
	BEACH(100,199),
	MONUMENTS(200,299),
	BUS(300,399),
	DINOSAURS(400,499),
	ELEPHANTS(500,599),
	FLOWERS(600,699),
	HORSES(700,799),
	MOUNTAINS(800,899),
	COURSES(900,999);
	
	private int minLine;
	private int maxLine;
	
	private Category(int minLine, int maxLine) {
		this.minLine = minLine;
		this.maxLine = maxLine;
	}

	/**
	 * @return the minId
	 */
	protected final int getMinLine() {
		return minLine;
	}

	/**
	 * @return the maxId
	 */
	protected final int getMaxLine() {
		return maxLine;
	}
	
	protected final int getIdRange() {
		return maxLine - minLine + 1;
	}
	
//	protected static final Category getByLine(int line) {
//		for (Category cat : values()) {
//			if (line >= cat.minLine && line <= cat.maxLine) {
//				return cat;
//			}
//		}
//		return null;
//	}
	
	protected static final Category getByIndex(int index) {
		return values()[index];
	}
	
	protected static final int size() {
		return values().length;
	}
	
	protected final int index() {
		return minLine/100;
	}
}
