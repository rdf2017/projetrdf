package projetRDFmain;
import java.io.EOFException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Utility class to read doubles from CSV files
 * @author maxim
 */
public class CSVReader {
	private static final int DEFAULT_LIST_SIZE = 1000;
	/**
	 * Separator between double values
	 */
	public String COLUMN_SEPARATOR = "\t";
	/** 
	 * Since this class parses only doubles, if a text is encountered, an error may be thrown.
	 * To prevent this, you can specify a CharSequence that if contained by the throwing text, will make the parse methods ignore it.
	 * A value of null indicates it throws without exception, a length of 0 indicates all text is ignored.
	 */
	public CharSequence IGNORED_TEXT_ERRORS = ".jpg";
	
	/**
	 * Reads an entire file and builds an array composed of as many arrays as there are lines (elements).
	 * Those latter arrays have as many cells as there are columns (dimensions) in the feature.
	 * @param filePath feature
	 * @return data for all input
	 * @throws IOException 
	 */
	public List<List<Double>> readAll(String filePath) throws IOException {
		final FileReader reader = new FileReader(filePath);
		final List<List<Double>> res = parse(reader, 0, DEFAULT_LIST_SIZE);
		reader.close();
		return res;
	}
	
	/**
	 * Reads the data from a file for a specified range of lines. 
	 * If range == 0, this is as if a single line is read. Use {@linkplain CSVReader#readLine(String, int)} instead.
	 * @param filePath
	 * @param startLine
	 * @param range
	 * @return feature data for all input in the range
	 * @throws IOException
	 */
	public List<List<Double>> readLines(String filePath, int startLine, int range) throws IOException {
		if (startLine < 0 || range < 0) {
			throw new IllegalArgumentException("startLine and range arguments must be positive !");
		}
		final FileReader reader = skipToLine(new FileReader(filePath), startLine);
		final List<List<Double>> res = parse(reader, startLine, range + startLine - 1);
		reader.close();
		return res;
	}
	
	/**
	 * Reads the data from a file at a given line.
	 * @param filePath
	 * @param line to read
	 * @return feature data at the give line
	 * @throws IOException
	 */
	public List<Double> readLine(String filePath, int line) throws IOException {
		final FileReader reader = skipToLine(new FileReader(filePath), line);
		final List<Double> res = parse(reader, line, line).get(0);
		reader.close();
		return res;
	}
	
	/**
	 * Skips as many characters as needed to reach the given line.
	 * @param reader
	 * @param line
	 * @return the given reader marked at the beginning of the give line
	 * @throws IOException
	 */
	private static FileReader skipToLine(FileReader reader, int line) throws IOException {
		if (line != 0) {	// skip lines if category is not the first one
			int lineSize = -1;
			int read;
			do {	// alphanum and space chars
				read = throwingRead(reader);
				lineSize++;
			} while ('\n' != read && '\r' != read);
			while ('\n' == read || '\r' == read) {	// newline chars
				throwingRead(reader);
				lineSize++;
			}
			if (line != 1) {
				reader.skip(lineSize * line);
			} else {
				reader.reset();
				reader.skip(lineSize);
			}
		}
		return reader;
	}
	
	private static int throwingRead(FileReader reader) throws IOException {
		final int read = reader.read();
		if (read == -1) {
			throw new EOFException("Unexpected end of file reached !");
		}
		return read;
	}
	
	/**
	 * Builds the ordered list of images with their data as doubles found in a file from minLine to maxLine
	 * @param reader 
	 * @param minLine
	 * @param maxLine
	 * @return the ordered list of images from the file
	 * @throws IOException
	 */
	private List<List<Double>> parse(FileReader reader, int minLine, int maxLine) throws IOException {
		final int range = maxLine - minLine + 1;
		final Scanner scan = new Scanner(reader);
		final ArrayList<List<Double>> images = new ArrayList<>(Math.min(DEFAULT_LIST_SIZE, range));
		
		int lineId = minLine;
		while (scan.hasNextLine() && lineId <= maxLine) {	// effectively read
			final String line = scan.nextLine();	
			try {
				images.add(parseLine(line));
			} catch (NumberFormatException e) {
				scan.close();
				throw e;
			}
			lineId++;
		}
		scan.close();
		return images;
	}
	
	/**
	 * Parses all doubles found in a line separated by tabulations.
	 * @param line to parse
	 * @return list of doubles
	 * @throws NumberFormatException thrown if a double can not be parsed and this isn't the first column (image name) 
	 */
	private List<Double> parseLine(String line) throws NumberFormatException {
		final Scanner lineScan = new Scanner(line);
		final List<Double> image = new ArrayList<>();
		lineScan.useDelimiter(COLUMN_SEPARATOR);
		while (lineScan.hasNext()) {
			final String toParse = lineScan.next();
			try {
				image.add(Double.valueOf(toParse));
			} catch(NumberFormatException e) {
				if (IGNORED_TEXT_ERRORS != null && (!toParse.contains(IGNORED_TEXT_ERRORS) || IGNORED_TEXT_ERRORS.equals(""))) {
					lineScan.close();
					throw e;
				}
			}
		}
		lineScan.close();
		return image;
	}
}
