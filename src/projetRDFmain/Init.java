package projetRDFmain;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Init {
	private static enum Mode { FEATURES,RECORD,PATHFEATURES };
	private Mode myMode;
	private String m_file;
	private String m_path;
	private List<String> m_featuresList;
	private final List<String> m_features;
	
	public Init() {
		myMode = Mode.RECORD;
		m_file = null;
		m_path = null;
		m_featuresList = null;
		m_features = new ArrayList<String>();
	}
	
	public void doAnythingYouLikeBabe(String[] args) {
		if (args.length == 0) {
			args = new String[] { "-h" };
		}
		for(String s : args) {
			if(s.equals("-h")) {
				System.out.println(" use -h for help.");
				System.out.println(" use -r to indicate the file name input.");
				System.out.println(" use -p to indicate the path to feature files input.");
				System.out.println(" use -f to list features (files) to activate.");
				System.exit(0);
			} else if(s.equals("-f")) {
				myMode = Mode.FEATURES;
			} else if(s.equals("-r")) {
				myMode = Mode.RECORD;
			} else if(s.equals("-p")) {
				myMode = Mode.PATHFEATURES;
			} else {
				if (Mode.RECORD.equals(myMode)) {
					if(m_file!=null) {
						System.err.println("File already set !");
					}
					m_file = s;
				} else if(Mode.FEATURES.equals(myMode)){
					m_features.add(s);
				} else if (Mode.PATHFEATURES.equals(myMode)){
					final File path = new File(s);
					if (!path.isDirectory()) {
						System.err.println(s + " is not a directory !");
						System.err.println(path.getAbsolutePath());
					} else {
						if (m_path != null) {
							System.err.println("Path already set !");
						}
						m_path = s;
						m_featuresList = Arrays.stream(path.listFiles())
								.map(f -> f.getName())
								.collect(Collectors.toList()
						);
					}
				}
			}
		}
		if (m_features.isEmpty() && !m_featuresList.isEmpty()) {
			m_features.addAll(m_featuresList);
		}
	}

	/**
	 * @return the m_file
	 */
	public String getM_file() {
		return m_file;
	}
	
	/**
	 * @return the m_file
	 */
	public String getM_path() {
		return m_path;
	}

	/**
	 * @return the m_features
	 */
	public List<String> getM_features() {
		return m_features;
	}
}
