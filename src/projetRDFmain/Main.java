package projetRDFmain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
	
	public static void main(String[] args) {
		final Init init = new Init();
		init.doAnythingYouLikeBabe(args);

		final FeaturesController fController = new FeaturesController(init.getM_features(), init.getM_path());
		fController.obtainAndCompileData();
		final Indexation indexAlgo = new Indexation(fController.getCompiledData());
		final int compareImage = Integer.valueOf(init.getM_file());
		final int nbClosestRecords = 5;
		
		
		final int[] closestRecords = indexAlgo.getClosestRecords(
			fController.getCompiledData().get(compareImage),
			nbClosestRecords
		);
		System.out.println("Les " + nbClosestRecords + " plus proches images de l'image " + compareImage + " sont :");
		System.out.println(Arrays.toString(closestRecords));
		System.out.println();
		
		confusionIndexation(fController,indexAlgo,nbClosestRecords);
		
		System.out.println("C'est fini pour l'indexation !\n\n");
		
		
		
		
		
		// partitionner la data en 5, en conservant les proportions de categories
		// pour chaque partition :
		//		apprentissage avec les 4 autres
		//		decision
		
		// [attendus][obtenus]
		final int[][] confusions = new int[Category.size()][Category.size()];
		
		// creation des partitions et ajout de l'information de categorie
		final int nbParts = 5;
		final List<Map<Category, List<List<Double>>>> parts = new ArrayList<>(nbParts);
		for (int partIdx = 0; partIdx < nbParts; partIdx++) {
			parts.add(new HashMap<>(Category.size()));
			for (Category cat : Category.values()) {
				parts.get(partIdx).put(cat, new ArrayList<>());
				final List<List<Double>> catFiles = parts.get(partIdx).get(cat);
				for (int lineIdx = cat.getMinLine() + (partIdx * (cat.getIdRange() / nbParts)); // premiere ligne pour la partition et categorie
						lineIdx <= cat.getMaxLine() - ((nbParts - partIdx - 1) * (cat.getIdRange() / nbParts)); // derniere...
						lineIdx++) {
					catFiles.add(fController.getCompiledData().get(lineIdx));
				}
			}
		}
		
		// cross validation pour chaque partition
		for (int decidingPartIdx = 0; decidingPartIdx < nbParts; decidingPartIdx++) {
			final Map<Category, List<List<Double>>> decidingPart = parts.get(decidingPartIdx);
			final Map<Category, List<List<Double>>> trainingParts = new HashMap<>(Category.size());
			// constitution de la partition d'apprentissage
			for (int trainingPartIdx = 0; trainingPartIdx < nbParts; trainingPartIdx++) {
				if (trainingPartIdx != decidingPartIdx) {
					for (Category cat : Category.values()) {
						trainingParts.putIfAbsent(cat, new ArrayList<>());
						trainingParts.get(cat).addAll(parts.get(trainingPartIdx).get(cat));
					}
				}
			}
			// decision
			final KNN knn = new KNN(trainingParts);
			for (Category cat: decidingPart.keySet()) {
				for (int decidingFileIdx = 0; decidingFileIdx < decidingPart.get(cat).size(); decidingFileIdx++) {
					final List<Double> decidingFile = decidingPart.get(cat).get(decidingFileIdx);
					final Category classRes = knn.computeNeighborhoodKNN(decidingFile, nbClosestRecords);

					confusions[cat.index()][classRes.index()]++;
				}
			}
		}

		System.out.println("Resultats de la classification :");
		System.out.println(Arrays.toString(Category.values()));
		for (int[] confusion : confusions) {
			System.out.println(Arrays.toString(confusion));
		}
	}
	
	public static void confusionIndexation(FeaturesController fController, Indexation indexationAlgo, int nbClosestRecords) {
		// pour chaques element on va tester si les elements les plus proches sont dans la meme classe
		float trueIsTrue = 0;
		float trueIsFalse = 0;
		float falseIsTrue = 0;
		float falseIsFalse = 0;
		List<List<Double>> data = fController.getCompiledData();
		for(int i=0; i <1000 ;i+=100) {
			for(int j = 0 ; j<100 ;j++) {
				int[] closestRecords = indexationAlgo.getClosestRecords(
						data.get(i+j),
						nbClosestRecords
					);
				for(int k : closestRecords) {
					if(k/100==i) {
						trueIsTrue +=1;
					}else {
						trueIsFalse +=1;
					}
					
				}
			}
		}
		System.out.println("La ressemblance est de : " + trueIsTrue/(500)  );
	}

}
