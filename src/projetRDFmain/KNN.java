package projetRDFmain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.util.Pair;

public class KNN {
	private final Map<Category, List<List<Double>>> learningRecords;
	
	public KNN(Map<Category, List<List<Double>>> learningRecords) {
		this.learningRecords = learningRecords;
	}
	
	public Category computeNeighborhoodKNN(List<Double> sample, int K) {
		final List<Pair<Category,Double>> clustersDistance = new ArrayList<>(learningRecords.size());
		
		for (Category cat : learningRecords.keySet()) {
			for (List<Double> lSample : learningRecords.get(cat)) {	// Pour chaque sample d�j� classifi� on calcule le couple Cluster,Distance avec le sample � classifier
				clustersDistance.add(new Pair<Category, Double>(cat, distance(sample, lSample)));
			}
		}
		
		clustersDistance.sort(new Comparator<Pair<Category, Double>>() {	// Tri dans l'ordre croissant des distances
			@Override
			public int compare(Pair<Category, Double> arg0, Pair<Category, Double> arg1) {
				return (int) (arg0.getValue() - arg1.getValue());
			}
		});

		final int[] clusterOccurrences = new int[Category.size()];	// Calcul du nombre d'occurrences des clusters parmi les K plus proches voisins
		for (int i = K - 1; i >= 0; i--) {
			clusterOccurrences[
			                   clustersDistance.get(i).getKey().index()
			                   ]++;
		}
		int bestCluster = 0;	// Choix du meilleur cluster
		for (int i = Category.size() - 1; i >= 0; i--) {
			if (clusterOccurrences[i] > clusterOccurrences[bestCluster]) 
				bestCluster = i;
		}
		return Category.getByIndex(bestCluster);
	}
	
	private static Double distance(List<Double> one, List<Double> two) {
		final Iterator<Double> it1 = one.iterator();
		final Iterator<Double> it2 = two.iterator();
		double res = 0d;
		while (it1.hasNext()) {
			res += Math.abs(it1.next() - it2.next());
		}
		return res;
	}
}
