package projetRDFmain;

import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;

/**
 * Class that corresponds to the indexation part of the project
 */
public class Indexation {
	private final List<List<Double>> records;
	
	/**
	 * @param records
	 */
	public Indexation(List<List<Double>> records) {
		this.records = records;
	}
	
	/**
	 * Finds the closest records to a given record. 
	 * @param in the record to compare with
	 * @param amount number of closest records to return
	 * @return the indices of the closest records
	 */
	public int[] getClosestRecords(List<Double> in, int amount) {
		// setup
		final List<Pair<Integer, Double>> closestRecords = new ArrayList<>(amount);
		for (int i = 0; i < amount; i++) {
			closestRecords.add(new Pair<Integer, Double>(-1, Double.MAX_VALUE));
		}
		
		for (int recordIdx = 0; recordIdx < records.size(); recordIdx++) {
			final List<Double> record = records.get(recordIdx);
			// compute distance sum
			double distSum = 0d;
			for (int dim = 0; dim < record.size(); dim++) {
				distSum += Math.abs(record.get(dim) - in.get(dim));
			}
			// compare with closest records from biggest to lowest
			for (int dim = closestRecords.size() - 1; dim >= 0; dim--) {
				if (distSum < closestRecords.get(dim).getValue()) {
					// move all records to the lowest side and add new closest record
					for (int dim2 = 1; dim2 <= dim; dim2++) {
						closestRecords.set(dim2-1, closestRecords.get(dim2));
					}
					closestRecords.set(dim, new Pair<Integer,Double>(recordIdx, distSum));
					break;
				}
			}
		}
		// return indices
		final int[] res = new int[amount];
		for (int i = 0; i < amount; i++) {
			res[i] = closestRecords.get(i).getKey();
		}
		return res;
	}
	
	/**
	 * @param pos
	 * @return the record
	 */
	public List<Double> getRecord(int pos) {
		return records.get(pos);
	}
}
